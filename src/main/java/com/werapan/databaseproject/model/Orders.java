/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author natthawadee
 */
public class Orders {
    private int id;
    private Date date;
    private double total;
    private int qty;
    private ArrayList<OrderDetail>orderDetails;

    public Orders(int id, Date date, double total, int qty, ArrayList<OrderDetail> orderDetails) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.qty = qty;
        this.orderDetails = orderDetails;
    }

    public Orders() {
        this.id = -1;
         this.orderDetails = new ArrayList<>();
         qty = 0;
         total =0.;
         date = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    //public ArrayList<OrderDetail> getOrderDetails() {
        //return orderDetails;
    //}

    public void setOrderDetails(ArrayList<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    @Override
    public String toString() {
        return "Orders{" + "id=" + id + ", date=" + date + ", total=" + total + ", qty=" + qty + "}";
    }
    
    public void addOrderDetail(OrderDetail orderDetail){
        orderDetails.add(orderDetail);
        total = orderDetail.getTotal();
        qty = qty + orderDetail.getQty();
    }

    public ArrayList<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void addOrderDetail(Product product, String productName,double productPrice, int qty) {
        OrderDetail orderDetail = new OrderDetail(product,productName,productPrice,qty,this);  
       this.addOrderDetail(orderDetail);
    }
     public void addOrderDetail(Product product, int qty) {
        OrderDetail orderDetail = new OrderDetail(product,product.getName(),product.getPrice(),qty,this);  
       this.addOrderDetail(orderDetail);
    }
      public static Orders fromRS(ResultSet rs) {
       Orders  order = new Orders();
        try {
             order.setId(rs.getInt("order_id"));
             order.setQty(rs.getInt("order_qty"));
             order.setTotal(rs.getDouble("order_total"));
             order.setDate(rs.getDate("order_date"));
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
             order.setDate(sdf.parse(rs.getString("order_date")));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Orders.class.getName()).log(Level.SEVERE, null, ex);
        }
        return order;
    }
}
